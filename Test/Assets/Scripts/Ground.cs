using UnityEngine;
using UnityEngine.EventSystems;

public class Ground : MonoBehaviour, IDragHandler
{
    private MeshCollider _collider;
    [SerializeField]
    private Camera _camera;

    private void Start()
    {
        _collider = GetComponent<MeshCollider>();
    }

    /// <summary> ���������� �������� ��������� ����� �� ����������� </summary>
    public Vector3 ChoosePoint()
    {
        var sizeX = _collider.bounds.max.x;
        var sizeZ = _collider.bounds.max.z;
        return new Vector3
        {
            x = Random.Range(sizeX * -1, sizeX),
            y = 0.5f,
            z = Random.Range(sizeZ * -1, sizeZ)
        };
    }

    public void OnDrag(PointerEventData eventData)
    {
        gameObject.transform.position += new Vector3
            (
                eventData.delta.x / 10, 
                0, 
                eventData.delta.y / 10
            );
    }
}
