using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _enemyPrefabs;
    private Ground _ground;

    private List<GameObject> _enemies;
    private int maxEnemies = 10;
    public int enemiesLevel = 1;

    private float _minBirthTime = 1f;
    private float _maxBirthTime = 5f;
    private float _birthTime;
    private float _timer;

    public event System.Action<int> UpdateEnemiesCountEvent;
    public event System.Action GameOverEvent;


    void Start()
    {
        _enemies = new List<GameObject>();
        _ground = FindObjectOfType<Ground>();
    }

    void Update()
    {
        if (_timer >= _birthTime)
        {
            _timer = 0;
            CreateEnemy();
        }
        if (_enemies.Count >= maxEnemies)
        {
            GameOverEvent?.Invoke();
        }
        _timer += Time.deltaTime;
    }

    /// <summary> ������� ����� �� ����������� </summary>
    private void CreateEnemy()
    {
        _birthTime = Random.Range(_minBirthTime, _maxBirthTime);

        var spawnPoint = _ground.ChoosePoint();
        var enemyPrefab = GetNextEnemy();
        var enemy = Instantiate(enemyPrefab, spawnPoint, enemyPrefab.transform.rotation);

        EnemyLevelUp(enemiesLevel, enemy.GetComponent<EnemyController>());
        UpdateEnemiesCountEvent?.Invoke(1);
        _enemies.Add(enemy);
    }

    /// <summary> �������� ����� ��� �������� </summary>
    private GameObject GetNextEnemy()
    {
        return _enemyPrefabs[Random.Range(0, _enemyPrefabs.Count)];
    }

    /// <summary> ��������� ���-�� ������ </summary>
    public void DestroyEnemy(GameObject enemy)
    {
        UpdateEnemiesCountEvent?.Invoke(-1);
        _enemies.Remove(enemy);
    }

    ///<summary> �������� ������� ������ </summary>
    private void EnemyLevelUp(int enemyLevel, EnemyController enemy)
    {
        for (int i = 0; i < enemyLevel - 1; i++)
        {
            enemy.NextLevel();
        }
    }
}
