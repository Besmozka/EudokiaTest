using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;
    [SerializeField]
    private Text _enemiesCountUI;
    [SerializeField]
    private Text _totalKillsUI;
    [SerializeField]
    private Text _enemiesLevelUI;

    private int _totalKills;

    private GameObject gamePanel;
    private GameObject pausePanel;
    private GameObject gameOverPanel;
    private GameObject mainMenuPanel;

    void Start()
    {
        gamePanel = gameObject.transform.Find("GamePanel").gameObject;
        gamePanel.SetActive(false);

        pausePanel = gameObject.transform.Find("PausePanel").gameObject;
        pausePanel.SetActive(false);

        gameOverPanel = gameObject.transform.Find("GameOverPanel").gameObject;
        gameOverPanel.SetActive(false);

        mainMenuPanel = gameObject.transform.Find("MainMenuPanel").gameObject;
        mainMenuPanel.SetActive(true);

        _gameManager.UpdateEnemiesCountEvent += UpdateUIEnemiesCount;
        _gameManager.GameOverEvent += GameOver;

        Time.timeScale = 0;
    }

    void Update()
    {
        
    }

    /// <summary> ��������� ���-�� �������� � ������ ������ </summary>
    private void UpdateUIEnemiesCount(int count)
    {
        var currentCount = Convert.ToInt32(_enemiesCountUI.text);
        currentCount += count;
        _enemiesCountUI.text = currentCount.ToString();

        if (count < 0)
        {
            _totalKills++;
            _totalKillsUI.text = _totalKills.ToString();
            if (_totalKills % 10 == 0)
            {
                _gameManager.enemiesLevel++;
                _enemiesLevelUI.text = _gameManager.enemiesLevel.ToString();
            }
        }
    }

    public void Play()
    {
        mainMenuPanel.SetActive(false);
        pausePanel.SetActive(false);
        gamePanel.SetActive(true);
        Time.timeScale = 1;
    }

    public void Pause()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void Settings()
    {

    }

    public void TopPlayers()
    {

    }

    private void GameOver()
    {
        gameOverPanel.SetActive(true);
        Time.timeScale = 0;
    }
  
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
