using UnityEngine;

public class Enemy
{
    public float Speed { get; private set; }
    public float StopDistance { get; private set; }
    public int Health { get; private set; }
    

    public Enemy(float speed, int health)
    {
        Speed = speed;
        StopDistance = 0.4f;
        Health = health;
    }

    /// <summary> ��������� ������ </summary>
    public void LevelUp(int health, int speed)
    {
        Health += health;
        Speed += speed;
    }

    /// <summary> ��������� ����� </summary>
    /// <param name="damage"> ���-�� ����������� ����� </param>
    public void TakeDamage(int damage)
    {
        Health -= damage;
    }
}
