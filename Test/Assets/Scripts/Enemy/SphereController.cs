using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SphereController : EnemyController
{
    private float _sphereSpeed = 10f;
    private int _sphereHealth = 2;

    public void Awake()
    {
        _enemy = new Enemy(_sphereSpeed, _sphereHealth);

    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }
    protected override void Die()
    {
        base.Die();
    }

    protected override void MoveTo(Vector3 point)
    {
        base.MoveTo(point);
    }

    protected override void NextMovePoint()
    {
        base.NextMovePoint();
    }

    public override void NextLevel()
    {
        base.NextLevel();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
    }
}
