using System;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class EnemyController : MonoBehaviour, IPointerEnterHandler
{
    protected Enemy _enemy;

    [SerializeField]
    private GameObject _dieEffect;

    private CharacterController _controller;
    private Ground _ground;
    private GameManager _gameManager;
    private Vector3 _movePoint;
    private Animator _animator;


    protected virtual void Start()
    {
        _controller = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _ground = FindObjectOfType<Ground>();
        _gameManager = FindObjectOfType<GameManager>();
        _movePoint = _ground.ChoosePoint();
    }
    
    protected virtual void Update()
    {
        var distance = Vector3.Distance(_movePoint, transform.position);
        if (distance > _enemy.StopDistance)
        {
            MoveTo(_movePoint);
        }
        else
        {
            NextMovePoint();
        }
    }

    /// <summary> ��������� � ����� </summary>
    protected virtual void MoveTo(Vector3 point)
    {
        _animator.SetInteger("AnimationState", (int)AnimationState.WalkState);
        var direction = point - transform.position;
        transform.LookAt(point);
        _controller.Move(direction.normalized * _enemy.Speed * Time.deltaTime);
    }

    /// <summary> ������� ��������� ����� </summary>
    protected virtual void NextMovePoint()
    {
        _movePoint = _ground.ChoosePoint();
    }

    /// <summary> ���������� ������ </summary>
    protected virtual void Die()
    {
        _animator.SetInteger("AnimationState", (int)AnimationState.DieState);
        Instantiate(_dieEffect, gameObject.transform.position, _dieEffect.transform.rotation);
        Destroy(gameObject);
    }

    /// <summary> �������� ��� ������� �� ������ </summary>
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        _animator.SetInteger("AnimationState", (int)AnimationState.DamageState);

        _enemy.TakeDamage(1);
        if (_enemy.Health <= 0)
        {
            _gameManager.DestroyEnemy(gameObject);
            Die();
        }
    }

    public virtual void NextLevel()
    {
        _enemy.LevelUp(health: 1, speed: 1);
    }    
}
