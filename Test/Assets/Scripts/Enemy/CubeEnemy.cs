using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CubeEnemy : EnemyController
{
    private float _cubeSpeed = 15f;
    private int _cubeHealth = 3;

    private void Awake()
    {
        _enemy = new Enemy(_cubeSpeed, _cubeHealth);
    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void Die()
    {
        base.Die();
    }

    protected override void MoveTo(Vector3 point)
    {
        base.MoveTo(point);
    }

    protected override void NextMovePoint()
    {
        base.NextMovePoint();
    }

    public override void NextLevel()
    {
        base.NextLevel();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
    }
}
